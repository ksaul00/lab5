package socialnetwork.UI;

import socialnetwork.domain.FriendshipRequest;
import socialnetwork.domain.Message;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.MonthValidator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.Service;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Console {
    Service service;

    public Console(Service service) {
        this.service = service;
    }

    private void showMenu(){
        System.out.println("\n");
        System.out.println("#######################################################   MENIU   #####################################################");
        System.out.println("1. ADD USER");
        System.out.println("2. REMOVE USER");
        System.out.println("3. REMOVE FRIEND");
        System.out.println("4. SHOW COMMUNITIES");
        System.out.println("5. SHOW THE MOST SOCIABLE COMUNITY");
        System.out.println("6. SHOW FRIENDSHIPS");
        System.out.println("7. SHOW FRIENDSHIPS OF AN USER FROM A SPECIFIC MONTH");
        System.out.println("8. SEND A FRIENDSHIP REQUEST");
        System.out.println("9. SHOW PENDING FRIENDSHIP REQUESTS FOR AN USER");
        System.out.println("10. APROVE/REJECT A FRIENDSHIP REQUEST");
        System.out.println("11. SHOW ALL FRIENDSHIP REQUESTS FOR AN USER");
        System.out.println("12. CHAT");
        System.out.println("13. Send a message to many people");
        System.out.println("a. SHOW ALL USER");
        System.out.println("x. EXIT");
        System.out.println("#######################################################################################################################");
        System.out.println("\n");
    }

    public void runMenu(){
        Scanner sc = new Scanner(System.in);
        boolean shouldRun = true;
        String opt;
        while(shouldRun){
            this.showMenu();
            System.out.print("Optiune aleasa: ");
            opt = new Scanner(System.in).nextLine();

            switch (opt)
            {
                case "1":
                    this.addUser();
                    break;
                case "2":
                    this.removeUser();
                    break;
                case "3":
                    this.removeFriend();
                    break;
                case "4":
                    this.showComunities();
                    break;
                case "5":
                    this.showTheMostSociableComunity();
                    break;
                case "6":
                    this.showAllFriendshipsOfAUser();
                    break;
                case "7":
                    this.showAllFriendshipsOfAUserForASpecificMonth();
                    break;
                case "8":
                    this.sendFriendRequest();
                    break;
                case "9":
                    this.showAllPendingFriendshipRequestForAnUser();
                    break;
                case "10":
                    this.aproveRejectaFriendshipRequest();
                    break;
                case "11":
                    this.showAllFriendshipRequestForAnUser();
                    break;
                case "12":
                    this.chat();
                    break;
                case "13":
                    this.sendAMessageToManyPeople();
                    break;
                case "sfs":
                    this.showAllFriendships();
                    break;
                case "a":
                    this.showAllUsers();
                    break;
                case "x":
                    System.out.println("bye, bye...");
                    shouldRun=false;
                    break;
                default:
                    System.out.println("Optiune gresita! Reincercati\n");
                    break;
            }
        }
    }

    private void sendAMessageToManyPeople() {
        try{
            System.out.print("Give the id of the user who want to send a message: ");
            Long id1 = new Scanner(System.in).nextLong();
            service.verifyUserWithAnIdExists(id1);
            User user1 = this.service.getUser(id1);
            System.out.print("give the ids of the destination users: ");
            String idsString = new Scanner(System.in).nextLine();
            addMessage(user1, this.service.convertIdListToUserList(idsString.split(" ")));
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void chat() {
        try{
            System.out.print("Give the id of the user who want to chat: ");
            Long id1 = new Scanner(System.in).nextLong();
            service.verifyUserWithAnIdExists(id1);
            User user1 = this.service.getUser(id1);
            System.out.print("give the id of the destination user: ");
            Long id2 = new Scanner(System.in).nextLong();
            service.verifyUserWithAnIdExists(id2);
            User user2 = this.service.getUser(id2);
            List<User> destinationList = new ArrayList<>();
            destinationList.add(user2);
            this.showMessages(id1, id2);
            messageMenu(user1, user2, destinationList);
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void messageMenu(User user1, User user2, List<User> destinationList){
        Boolean shoulRun = true;
        String opt;
        while(shoulRun){
            System.out.println("\n1. Send a message");
            System.out.println("2. Reply a message");
            System.out.println("x. Exit Chat");
            System.out.print("Option: ");
            opt = new Scanner(System.in).next();
            switch (opt) {
                case "1":
                    this.addMessage(user1, destinationList);
                    break;
                case "2":
                    this.replyMessage(user1, user2, destinationList);
                    break;
                case "x":
                    shoulRun = false;
                    break;
                default:
                    System.out.println("Unknown option! Retry\n");
                    break;
            }
        }
    }

    private void addMessage(User user1, List<User> destinationList){
        System.out.print("Message: ");
        String message = new Scanner(System.in).nextLine();
        Message message1 = this.service.addMessage(user1, destinationList, message, LocalDateTime.now(), 0L);
        System.out.println("(" + message1.getDate() + ") " + message1.getFrom().getLastName() + " " + message1.getFrom().getFirstName() + ": " + message1.getMessage());
    }

    private void replyMessage(User user1, User user2, List<User> destinationList){
        System.out.print("Give the id of the message whitch you walt to reply: ");
        Long idMessage = new Scanner(System.in).nextLong();
        Message mesagetoReply = this.service.getMessage(idMessage);
        if(mesagetoReply==null){
            throw new ValidationException("The message with id: " + idMessage + " does not exist in this conversation!");
        }
        if(!((mesagetoReply.getTo().contains(user1) && mesagetoReply.getFrom().equals(user2)) || (mesagetoReply.getTo().contains(user2) && mesagetoReply.getFrom().equals(user1)))){
            throw new ValidationException("The message with id: " + idMessage + " does not exist in this conversation!");
        }
        System.out.print("Message: ");
        String messageString = new Scanner(System.in).nextLine();
        Message message = this.service.addMessage(user1, destinationList, messageString, LocalDateTime.now(), idMessage);
        System.out.println("(" + mesagetoReply.getDate() + ") " + mesagetoReply.getFrom().getLastName() + " " + mesagetoReply.getFrom().getFirstName() + ": " + mesagetoReply.getMessage());
        System.out.println("        You Replyed: (" + message.getDate() + ") " + message.getFrom().getLastName() + " " + message.getFrom().getFirstName() + ": " + message.getMessage());
    }

    private void showMessages(Long id1, Long id2){
        Boolean messagesExist = false;
        System.out.println("\n###########################   CHAT   ###########################");
        for(Message msg: this.service.getMessagesBetween(id1, id2)){
            if(msg.getReply().equals(0l)){
                System.out.println(msg.getId() + ") (" + msg.getDate() + ") " + msg.getFrom().getLastName() + " " + msg.getFrom().getFirstName() + ": " + msg.getMessage());
                messagesExist = true;
            }
            else{
                System.out.println(msg.getId() + ") (" + msg.getDate() + ") " + msg.getFrom().getLastName() + " " + msg.getFrom().getFirstName() +" replyed: " + msg.getMessage());
                Message replyedMessage = this.service.getMessage(msg.getReply());
                System.out.println("            To this message: " + replyedMessage.getId() + ") (" + replyedMessage.getDate() + ") " + replyedMessage.getFrom().getLastName() + " " + replyedMessage.getFrom().getFirstName() + ": " + replyedMessage.getMessage());
                messagesExist = true;
            }
        }
        if(!messagesExist){
            System.out.println("                           Empty chat!");
        }
    }

    private void aproveRejectaFriendshipRequest() {
        try{
            System.out.print("Give the id of the user who want to see his Pending Friendship Requests: ");
            Long id = new Scanner(System.in).nextLong();
            if(this.service.getUser(id)==null){
                throw new ValidationException("The user with id: " + id + " does not exist!");
            }
            boolean userHasRequests = false;
            if(((Collection<?>) this.service.getPendingFriendshipRequestsOfAnUser(id)).size()==0){
                throw new ValidationException("The user with id: " + id + " has no friend requests!");
            }
            for(FriendshipRequest friendshipRequest: this.service.getPendingFriendshipRequestsOfAnUser(id)){
                System.out.println("Request id=" + friendshipRequest.getId() + " User: " + this.service.getUser(friendshipRequest.getIdSource()).getLastName() + " " + this.service.getUser(friendshipRequest.getIdSource()).getFirstName() + " sent you a friendship request in " + friendshipRequest.getDate().toString() + " STATUS: "+ friendshipRequest.getStatusFriendshipRequest().toString());
            }
            System.out.print("Which friendship request this user want to accept/reject (give id): ");
            Long id1 = new Scanner(System.in).nextLong();
            FriendshipRequest fr = this.service.getFriendshipRequest(id1);
            if(fr==null){
                throw new ValidationException("The Friendship Request with id: " + id1 + " does not exist!");
            }
            menuForAproveReject(fr);
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void menuForAproveReject(FriendshipRequest fr){
        System.out.println("1. APPROVE");
        System.out.println("2. REJECT");
        System.out.print("User's option: ");
        String option = new Scanner(System.in).nextLine();
        if(option.equals("1")){
            this.service.addFriend(fr.getIdSource(), fr.getIdDestination());
            this.service.updateStatusFriendshipRequest(fr, "APPROVED");
            System.out.println("\nFriendship between: ");
            System.out.println(this.service.getUser(fr.getIdSource()).toString());
            System.out.println("and");
            System.out.println(this.service.getUser(fr.getIdDestination()).toString());
            System.out.println("added Succesfully!");
        }
        else if(option.equals("2")){
            this.service.updateStatusFriendshipRequest(fr, "REJECTED");
            System.out.println("Frienship request form " + this.service.getUser(fr.getIdSource()).getLastName() + " " +this.service.getUser(fr.getIdSource()).getFirstName() + " was rejected!");
        }
        else{
            throw new ValidationException("Invalid option!");
        }
    }

    private void showAllPendingFriendshipRequestForAnUser() {
        try{
            System.out.print("Give the id of the user who want to see his Pending Friendship Requests: ");
            Long id = new Scanner(System.in).nextLong();
            service.verifyUserWithAnIdExists(id);
            if(((Collection<?>) this.service.getPendingFriendshipRequestsOfAnUser(id)).size()==0){
                throw new ValidationException("The user with id: " + id + " has no friend requests!");
            }
            for(FriendshipRequest friendshipRequest: this.service.getPendingFriendshipRequestsOfAnUser(id)){
                System.out.println("Request id=" + friendshipRequest.getId() + " User: " + this.service.getUser(friendshipRequest.getIdSource()).getLastName() + " " + this.service.getUser(friendshipRequest.getIdSource()).getFirstName() + " sent you a friendship request in " + friendshipRequest.getDate().toString() + " STATUS: "+ friendshipRequest.getStatusFriendshipRequest().toString());
            }
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void sendFriendRequest() {
        try{
            System.out.print("Give the id of the user who wand to send a friend requests: ");
            Long id1 = new Scanner(System.in).nextLong();
            if(this.service.getUser(id1)==null){
                throw new ValidationException("The user with id: " + id1 + " does not exist!");
            }
            System.out.print("Give the id of the user who will recive the request: ");
            Long id2 = new Scanner(System.in).nextLong();
            if(this.service.getUser(id2)==null){
                throw new ValidationException("The user with id: " + id2 + " does not exist!");
            }
            FriendshipRequest friendshipRequest = this.service.sendFriendShipRequest(id1, id2, "PENDING", LocalDate.now());
            System.out.println(friendshipRequest.toString() + " was added succesfully!");
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void showAllFriendshipRequestForAnUser() {
        try{
            System.out.print("Give the id of the user who want to see his Friendship Requests: ");
            Long id = new Scanner(System.in).nextLong();
            service.verifyUserWithAnIdExists(id);
            if(((Collection<?>) this.service.getFriendshipRequestsOfAnUser(id)).size()==0){
                throw new ValidationException("The user with id: " + id + " has no friend requests!");
            }
            for(FriendshipRequest friendshipRequest: this.service.getFriendshipRequestsOfAnUser(id)){
                System.out.println("Request id=" + friendshipRequest.getId() + " User: " + this.service.getUser(friendshipRequest.getIdSource()).getLastName() + " " + this.service.getUser(friendshipRequest.getIdSource()).getFirstName() + " sent you a friendship request in " + friendshipRequest.getDate().toString() + " STATUS: "+ friendshipRequest.getStatusFriendshipRequest().toString());
            }
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void showAllFriendshipsOfAUserForASpecificMonth() {
        try{
            if(this.service.getUsersSize()==0){
                System.out.println("There are no users to check their friends list");
            }
            else {
                System.out.print("Give id of the user who you want to see his/her friends: ");
                Long id = new Scanner(System.in).nextLong();
                service.verifyUserWithAnIdExists(id);
                if (this.service.getUser(id).getFriends().isEmpty())
                    throw new ValidationException("This user has no friends! ");
                else
                    System.out.print("Give the month of the friendships (ex: JANUARY, only uppercase letters): ");
                    String month = new Scanner(System.in).nextLine();

                    //TODO
                    new MonthValidator().validate(month);
                    if(((Collection<?>) this.service.getAllFriendshipsOfAnUserForASpecificMonth(id, month)).size()==0){
                        throw new ValidationException("The user with id: " + id + " has no friendShips in " + month.toString() + "!");
                    }
                    for (Tuple<User, LocalDate> tuplu : this.service.getAllFriendshipsOfAnUserForASpecificMonth(id, month)) {
                        System.out.println(tuplu.getLeft().getLastName() + " | " + tuplu.getLeft().getFirstName() + " | " + tuplu.getRight().toString());
                    }
            }

        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void showAllFriendshipsOfAUser() {

        try{
            if(this.service.getUsersSize()==0){
                System.out.println("There are no users to check their friends list");
            }
            else {
                System.out.print("Give id of the user who you want to see his/her friends: ");
                Long id = new Scanner(System.in).nextLong();
                service.verifyUserWithAnIdExists(id);
                if (this.service.getUser(id).getFriends().isEmpty())
                    System.out.println("This user has no friends! ");
                else
                    for (Tuple<User, LocalDate> tuplu : this.service.getAllFriendshipsOfAnUser(id)) {
                        System.out.println(tuplu.getLeft().getLastName() + " | " + tuplu.getLeft().getFirstName() + " | " + tuplu.getRight().toString());
                    }
            }

        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void showTheMostSociableComunity() {
        try{
            List<List<Long>> comunities = this.service.findTheMostSociableComunity();
            if(comunities.isEmpty()){
                System.out.println("Here are no Comunities!");
            }
            else{
                System.out.println("The most sociable Comunity/Comunities is/are: ");
                for(int i=0;i<comunities.size();i++){
                    System.out.println((i+1) + "): ");
                    for(int j=0;j<comunities.get(i).size();j++) {
                        System.out.println(this.service.getUser(comunities.get(i).get(j)).toString());
                    }
                    System.out.println("\n ");
                }
            }
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }

    }

    private void showComunities() {
        try{
            List<List<Long>> comunities = this.service.getComunities();
            if(comunities.isEmpty()){
                System.out.println("Here are no Comunities!");
            }
            else{
                System.out.println(comunities.size() +  " comunityes was founded: \n");

                for(int i=0;i<comunities.size();i++){
                    System.out.println("Comunity "+ (i+1) + ": ");
                    for(int j=0;j<comunities.get(i).size();j++) {
                        System.out.println(this.service.getUser(comunities.get(i).get(j)).toString());
                    }
                    System.out.println("\n ");
                }
            }
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }

    }

    private void showAllFriendsOfAUser() {
        try{
            if(this.service.getUsersSize()==0){
                System.out.println("There are no users to check their friends list");
            }
            else{
                System.out.print("Give id of the user who's friends list do you want to see: ");
                Long id = new Scanner(System.in).nextLong();
                List<User> listaPrieteni = this.service.giveFriendsOfAnUser(id);
                if(listaPrieteni.size()==0){
                    System.out.println("This user has no friends");
                }
                else{
                    System.out.println("Friends of user " + this.service.getUser(id) + " are:");
                    for(User user: listaPrieteni){
                        System.out.println(user.toString());
                    }
                }
            }
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void showAllFriendships() {
        this.service.getAllFriendships().forEach(System.out::println);
    }

    private void addUser(){
        try{
            String firstName, lastName, userName, password;
            System.out.print("Give userName (at least 5 characters, at least 3 letters, can't contains spaces): ");
            userName = new Scanner(System.in).nextLine();
            if (service.getUserByUserName(userName)!=null){
                throw new ValidationException("An user with UserName" + userName + "already exist!");
            }
            System.out.print("Give firstName (only letters can be used and only the first one is upper): ");
            firstName = new Scanner(System.in).nextLine();
            System.out.print("Give lastName (only letters can be used and only the first one is upper): ");
            lastName = new Scanner(System.in).nextLine();
            System.out.print("Give password (min 8 characters, min 1 upper letters, min 3 letters, min 2 digit numbers): ");
            password = new Scanner(System.in).nextLine();

            User task = this.service.addUser(userName, firstName, lastName, password);
            System.out.println("UserName: " + task.getUsername() + " User: " + task.getFirstName() + " " + task.getLastName() + " succesfully added!");
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
    }

    private void showAllUsers(){
        if(this.service.getUsersSize()==0) System.out.println("Sorry! There is no user");
        else
        {
            System.out.println("The users are: ");
            this.service.getAllUsers().forEach(System.out::println);
        }
    }

    private void removeUser(){
        try{
            if(this.service.getUsersSize()==0){
                System.out.println("There are no users to be removed");
            }
            else{
                System.out.print("Give User's id who you want to remove: ");
                Long id = new Scanner(System.in).nextLong();
                service.deleteUserFromFriendships(id);
                User task = this.service.removeUser(id);
                System.out.println("User " + task + " succesfully removed!");
            }
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void addFriend(){
        try{
            System.out.print("Whitch user whant to add a friend? Give id: ");
            Long id1 = new Scanner(System.in).nextLong();
            System.out.print("Whitch user he/she whants to be friend with? Give id: ");
            Long id2 = new Scanner(System.in).nextLong();
            this.service.addFriend(id1, id2);
            System.out.println("\nFirendship between: ");
            System.out.println(this.service.getUser(id1).toString());
            System.out.println("and");
            System.out.println(this.service.getUser(id2).toString());
            System.out.println("added Succesfully!");
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

    private void removeFriend(){
        try{
            System.out.print("Whitch user whant to remove a friend? Give id: ");
            Long id1 = new Scanner(System.in).nextLong();
            System.out.print("Whitch user he/she whants to remove? Give id: ");
            Long id2 = new Scanner(System.in).nextLong();
            this.service.removeFriend(id1, id2);
            System.out.println(this.service.getUser(id1));
            System.out.println("and");
            System.out.println(this.service.getUser(id2));
            System.out.println("Are no friends anymore!");
        }
        catch (ValidationException ve){
            System.out.println("Validation Exception: " + ve.getMessage());
        }
        catch (InputMismatchException ime){
            System.out.println("InputMismatchException: Id-ul trebuie sa fie un numar intreg pozitiv! ");
        }
    }

}
