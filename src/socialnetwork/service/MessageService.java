package socialnetwork.service;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.repository.Repository;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageService {
    private Repository<Long, Message> repoMessage;

    public MessageService(Repository<Long, Message> repoMessage) {
        this.repoMessage = repoMessage;
    }

    public Message addMessage(User from, List<User> to, String message, LocalDateTime date, Long reply){
        Message newMessage = new Message(from, to, message, date, reply);
        Message task = repoMessage.save(newMessage);
        return task;
    }

    public Iterable<Message> getAll() {
        return repoMessage.findAll();
    }

    public Message getMessage(Long id){
        for(Message msg: this.getAll())
            if(msg.getId().equals(id))
            {
                return msg;
            }
        return null;
    }

    public int getMessagesSize() {
        return this.repoMessage.getSize();
    }

    public Iterable<Message> getMessagesBetween(User user1, User user2) {
        Predicate<Message> messagePredicate = x-> (x.getTo().contains(user1) && x.getFrom().equals(user2)) || (x.getTo().contains(user2) && x.getFrom().equals(user1));
        Comparator<Message> messageComparator = (m1, m2) -> m1.getDate().compareTo(m2.getDate());
        List<Message> messageList = StreamSupport.stream(this.getAll().spliterator(), false)
                .filter(messagePredicate)
                .sorted(messageComparator)
                .collect(Collectors.toList());
        return messageList;
    }

    public void deleteMessagesOf(User user1) {
        Predicate<Message> messagePredicate = x-> (x.getTo().contains(user1)) || ( x.getFrom().equals(user1));
        List<Message> messageList = StreamSupport.stream(this.getAll().spliterator(), false)
                .filter(messagePredicate)
                .collect(Collectors.toList());
        for(Message msg: messageList){
            this.removeMessage(msg);
        }
    }

    private void removeMessage(Message msg) {
        this.repoMessage.delete(msg.getId());
    }
}
