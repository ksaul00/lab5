package socialnetwork.service;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;

import java.util.*;

public class UserService {
    private Repository<Long, User> repoUser;

    public UserService(Repository<Long, User> repoUser) {
        this.repoUser = repoUser;
    }

    public User addUser(String username, String firstName, String lastName, String password) {
        User user = new User(username, firstName, lastName, password);
        if(this.repoUser.getSize()==0){
            user.setId(0L);
        }
        else {
            user.setId(Collections.max(this.repoUser.getIDs())+1L);
        }
        User task = repoUser.save(user);
        return task;
    }

    public Iterable<User> getAll() {
        return repoUser.findAll();
    }

    public User removeUser(Long id){
        User task = this.repoUser.delete(id);
        return task;
    }

    public Set<Long> getIDs(){
        return this.repoUser.getIDs();
    }

    public void addFriend(Long id1, Long id2){
       if(id1.equals(id2)){
            throw new ValidationException("The user can not be his own friend!");
       }
       User user1 = getUser(id1);
       User user2 = getUser(id2);
       if(user1==null){
           throw new ValidationException("The user whith id: " + id1 + "does not exist!");
       }
        if(user2==null){
            throw new ValidationException("The user whith id: " + id2 + "does not exist!");
        }
       if(user1.getFriends().contains(user2)){
           throw new ValidationException("These Users are already friends!");
       }
       user1.addFriend(user2);
       user2.addFriend(user1);
       this.repoUser.update(user1);
       this.repoUser.update(user2);
    }

    public User getUser(Long id){
        for(User user: this.getAll())
            if(user.getId().equals(id))
            {
                return user;
            }
        return null;
    }

    public void removeFriend(Long id1, Long id2) {
       if(id1.equals(id2)){
            throw new ValidationException("The user isn't his own friend!");
       }
       User user1 = getUser(id1);
       User user2 = getUser(id2);
        if(user1==null){
            throw new ValidationException("The user whith id: " + id1 + " does not exist!");
        }
        if(user2==null){
            throw new ValidationException("The user whith id: " + id2 + " does not exist!");
        }
       if(!user1.isFriend(user2)){
            throw new ValidationException("These users are not friends");
       }
        user1.removeFriend(user2);
        user2.removeFriend(user1);
        this.repoUser.update(user1);
        this.repoUser.update(user2);
    }

    public void deleteUserFromFriendships(Long id) {


        User toBeRemoved = this.getUser(id);
        if(toBeRemoved==null){
            throw new ValidationException("The user whith id: " + id + "does not exist!");
        }
        for(User user: this.repoUser.findAll()){
                if(user.getFriends().contains(toBeRemoved)){
                    user.getFriends().remove(toBeRemoved);
                }
        }
    }

    public List<List<Long>> findTheMostSociableComunity(List<List<Long>> comunitati) {
        List<List<Long>> celeMaiMariComunitati = new ArrayList<>();
        List<Long> comunitateMaxima = new ArrayList<>();
        if(!comunitati.isEmpty()){
            comunitateMaxima = comunitati.get(0);
            for(List<Long> comunitate: comunitati){
                if(comunitate.size()>comunitateMaxima.size()){
                    comunitateMaxima=comunitate;
                }
            }
            for(List<Long> comunitate: comunitati){
                if(comunitate.size()==comunitateMaxima.size()){
                    celeMaiMariComunitati.add(comunitate);
                }
            }
            return celeMaiMariComunitati;
        }
        else{
            throw new ValidationException("There are no Communities!");
        }
    }

    public int getUsersSize() {
        return this.repoUser.getSize();
    }

    public User getUserbyUserName(String username) {
        for(User user: this.getAll()){
            if(user.getUsername().equals(username)){
                return user;
            }
        }
        return null;
    }
}
