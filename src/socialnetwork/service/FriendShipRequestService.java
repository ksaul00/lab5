package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendShipRequestService {
    private Repository<Long, FriendshipRequest> repoFriendshipRequest;

    public FriendShipRequestService(Repository<Long, FriendshipRequest> repoFriendshipRequest) {
        this.repoFriendshipRequest = repoFriendshipRequest;
    }

    public FriendshipRequest addFriendShipRequest(Long idSource, Long idDestination, String status, LocalDate date) {
        FriendshipRequest friendshipRequest = new FriendshipRequest(idSource, idDestination, status, date);
        FriendshipRequest task = repoFriendshipRequest.save(friendshipRequest);
        return task;
    }

    public Iterable<FriendshipRequest> getAll() {
        return repoFriendshipRequest.findAll();
    }

    public FriendshipRequest removeFriendshipRequest(Long id) {
        FriendshipRequest task = this.repoFriendshipRequest.delete(id);
        return task;
    }

    public FriendshipRequest getFriendshipRequest(Long id) {
        return this.repoFriendshipRequest.findOne(id);
    }

    public int getFriendshipRequestsSize() {
        return this.repoFriendshipRequest.getSize();
    }

    public FriendshipRequest getPendingFriendshipRequestbySourceAndDestination(Long idSource, Long idDestination) {
        for (FriendshipRequest fr : this.getAll()) {
            if ((fr.getIdSource().equals(idSource) && fr.getIdDestination().equals(idDestination) && fr.getStatusFriendshipRequest().equals(StatusFrierndshipRequest.PENDING))
                    || (fr.getIdSource().equals(idDestination) && fr.getIdDestination().equals(idSource) && fr.getStatusFriendshipRequest().equals(StatusFrierndshipRequest.PENDING))) {
                return fr;
            }
        }
        return null;
    }

    public void updateStatusFriendshipRequest(FriendshipRequest newFR) {
        this.repoFriendshipRequest.update(newFR);
    }

    public void deleteFriendshipRequestOf(User user) {
        Predicate<FriendshipRequest> messagePredicate = x -> (x.getIdSource().equals(user.getId())) || (x.getIdDestination().equals(user.getId()));
        List<FriendshipRequest> freindShipRequesttoDelete = StreamSupport.stream(this.getAll().spliterator(), false)
                .filter(messagePredicate)
                .collect(Collectors.toList());
        for (FriendshipRequest fr : freindShipRequesttoDelete) {
            this.removeFriendshipRequest(fr.getId());
        }
    }
}
