package socialnetwork.service;

import socialnetwork.domain.FriendShip;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FriendShipService {
    private Repository<Tuple<Long, Long>, FriendShip> repo;

    public FriendShipService(Repository<Tuple<Long, Long>, FriendShip> repo) {
        this.repo = repo;
    }

    public FriendShip addFriendShip(Long id1, Long id2){
        if(id1.compareTo(id2) > 0){
            Long aux;
            aux = id1;
            id1 = id2;
            id2 = aux;
        }
        if(id1.equals(id2)){
            throw new ValidationException("The user can not be his own friend!");
        }
        FriendShip friendShip = new FriendShip(LocalDate.now());
        friendShip.setId(new Tuple<Long, Long>(id1, id2));

    if(this.getFriendShip(new Tuple<Long, Long>(id1, id2))!=null) {
            throw new ValidationException("This friendship already exist!");
        }
        this.repo.save(friendShip);
        return friendShip;
    }

    public Iterable<FriendShip> getAll() {
        return this.repo.findAll();
    }

    public FriendShip getFriendShip(Tuple<Long, Long> id){
        //TODO
        FriendShip friendShip = new FriendShip();
        friendShip.setId(id);
        for(FriendShip friendShip1: this.getAll()){
            if(friendShip.equals(friendShip1)){
                return friendShip1;
            }
        }
        return null;
    }

    public void removeFriendship(Long id1, Long id2) {
        if(id1.equals(id2)){
            throw new ValidationException("The user isn't his own friend!");
        }

        //TODO
        FriendShip friendShip = new FriendShip();
        friendShip.setId(new Tuple<Long, Long>(id1, id2));

        if(this.getFriendShip(new Tuple<Long, Long>(id1, id2))==null) {
            throw new ValidationException("This friendship does not exist!");
        }

        this.repo.delete(new Tuple<Long, Long>(id1, id2));
        this.repo.delete(new Tuple<Long, Long>(id2, id1));
    }

    public void deleteUserFromFriendships(Long id) {
        List<FriendShip> prieteniiCareTrebuieSterse = new ArrayList<FriendShip>();
        for(FriendShip friendShip: this.getAll()){
            if(friendShip.getId().getLeft().equals(id) || friendShip.getId().getRight().equals(id)){
                prieteniiCareTrebuieSterse.add(friendShip);
            }
        }

        for(int i=0;i<prieteniiCareTrebuieSterse.size();i++){
            Tuple<Long, Long> iddeSters =prieteniiCareTrebuieSterse.get(i).getId();
            this.removeFriendship(iddeSters.getLeft(), iddeSters.getRight());
        }
    }

    private void addInListFriendsOf(Long id, List<Long> comunity){
        for(FriendShip fr: this.getAll()){
            if(fr.getId().getLeft().equals(id) && !comunity.contains(fr.getId().getRight())){
                comunity.add(fr.getId().getRight());
            }
            if(fr.getId().getRight().equals(id) && !comunity.contains(fr.getId().getLeft())){
                comunity.add(fr.getId().getLeft());
            }
        }
    }

    public List<List<Long>> getComunities() {

        List<List<Long>> comunities = new ArrayList<>();
        List<Tuple<Long, Long>> tupluri = new ArrayList<>();
        tupluri.addAll(this.repo.getIDs());
        Collections.sort(tupluri, new Comparator<Tuple<Long, Long>>() {
            @Override
            public int compare(Tuple<Long, Long> o1, Tuple<Long, Long> o2) {
                return o1.getLeft().compareTo(o2.getLeft());
            }
        });

        boolean isAdded = false;
        for(int i=0;i<tupluri.size();i++){
            isAdded = false;
            for(int j=0;j<comunities.size();j++){
                if(comunities.get(j).contains(tupluri.get(i).getLeft())){
                    if(!comunities.get(j).contains(tupluri.get(i).getRight()))
                    {
                        comunities.get(j).add(tupluri.get(i).getRight());
                        isAdded = true;
                        addInListFriendsOf(tupluri.get(i).getRight(), comunities.get(j));
                        break;
                    }
                }
                if(comunities.get(j).contains(tupluri.get(i).getRight())){
                    if(!comunities.get(j).contains(tupluri.get(i).getLeft()))
                    {
                        comunities.get(j).add(tupluri.get(i).getLeft());
                        isAdded = true;
                        addInListFriendsOf(tupluri.get(i).getLeft(), comunities.get(j));
                        break;
                    }
                }
                if(comunities.get(j).contains(tupluri.get(i).getRight())){
                    if(comunities.get(j).contains(tupluri.get(i).getLeft()))
                    {
                        isAdded = true;
                        break;
                    }
                }
            }
            if(!isAdded){
                ArrayList<Long> newCommunity = new ArrayList<>();
                newCommunity.add(tupluri.get(i).getLeft());
                newCommunity.add(tupluri.get(i).getRight());
                addInListFriendsOf(tupluri.get(i).getLeft(), newCommunity);
                comunities.add(newCommunity);
            }

        }


        return comunities;
    }
}
