package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Service {
    private UserService userService;
    private FriendShipService friendShipService;
    private FriendShipRequestService friendShipRequestService;
    private MessageService messageService;

    public Service(UserService userService, FriendShipService friendShipService, FriendShipRequestService friendShipRequestService, MessageService messageService) {
        this.userService = userService;
        this.friendShipService = friendShipService;
        this.friendShipRequestService = friendShipRequestService;
        this.messageService = messageService;
    }

    public Iterable<User> getAllUsers() {
        return this.userService.getAll();
    }

    public User addUser(String username, String firstName, String lastName, String password) {
        return this.userService.addUser(username, firstName, lastName, password);
    }

    public User removeUser(Long id) {
        this.messageService.deleteMessagesOf(this.getUser(id));
        this.friendShipRequestService.deleteFriendshipRequestOf(this.getUser(id));
        return this.userService.removeUser(id);
    }

    public void addFriend(Long id1, Long id2) {
        this.userService.addFriend(id1, id2);
        this.friendShipService.addFriendShip(id1, id2);
    }

    public Iterable<FriendShip> getAllFriendships() {
        return this.friendShipService.getAll();
    }

    public void populateFriends(){
        for (FriendShip friendShip: this.friendShipService.getAll()){
            this.userService.addFriend(friendShip.getId().getLeft(), friendShip.getId().getRight());
        }
    }

    public void deleteUserFromFriendships(Long id) {
        this.userService.deleteUserFromFriendships(id);
        this.friendShipService.deleteUserFromFriendships(id);
    }

    public void removeFriend(Long id1, Long id2) {
        this.userService.removeFriend(id1, id2);
        this.friendShipService.removeFriendship(id1, id2);
    }

    public User getUser(Long id1) {
        return this.userService.getUser(id1);
    }

    public List<User> giveFriendsOfAnUser(Long id) {
        User user = this.userService.getUser(id);
        if(user==null){
            throw new ValidationException("The user with id " + id + " does not exist!");
        }
        return user.getFriends();
    }

    public List<List<Long>> getComunities() {
        return this.friendShipService.getComunities();
    }

    public List<List<Long>> findTheMostSociableComunity() {
        return this.userService.findTheMostSociableComunity(this.getComunities());
    }

    public int getUsersSize() {
        return this.userService.getUsersSize();
    }

    public User getUserByUserName(String username) {
        return this.userService.getUserbyUserName(username);
    }

    public Iterable<Tuple<User, LocalDate>> getAllFriendshipsOfAnUser(Long id) {
        Predicate<FriendShip> predicateFilterFriendships = x-> x.getId().getLeft().equals(id) || x.getId().getRight().equals(id);
        List<Tuple<User, LocalDate>> friendshipsToReturn = StreamSupport.stream(this.friendShipService.getAll().spliterator(), false)
                .filter(predicateFilterFriendships)
                .map(x->
                {
                    if (x.getId().getRight().equals(id)) x.setId(new Tuple<Long, Long>(id, x.getId().getLeft()));
                    return new Tuple<User, LocalDate> (this.userService.getUser(x.getId().getRight()), x.getDate());
                })
                .collect(Collectors.toList());
        return friendshipsToReturn;
    }

    public Iterable<Tuple<User, LocalDate>> getAllFriendshipsOfAnUserForASpecificMonth(Long id, String month) {
        Predicate<Tuple<User, LocalDate>> predicateFilterFriendshipsByMonth = x-> x.getRight().getMonth().toString().compareTo(month) == 0;
        List<Tuple<User, LocalDate>> friendshipsToReturn = StreamSupport.stream(this.getAllFriendshipsOfAnUser(id).spliterator(), false)
                .filter(predicateFilterFriendshipsByMonth)
                .collect(Collectors.toList());
        return friendshipsToReturn;
    }

    public FriendshipRequest sendFriendShipRequest(Long idSource, Long idDestination, String status, LocalDate date){
        if(idSource.equals(idDestination)){
            throw new ValidationException("The user can not be his own friend!");
        }
        if(this.friendShipService.getFriendShip(new Tuple<Long, Long>(idSource, idDestination))!=null) {
            throw new ValidationException("These users are already friends!");
        }
        if(this.friendShipRequestService.getPendingFriendshipRequestbySourceAndDestination(idSource, idDestination)!=null){
            throw new ValidationException("There is already a PENDING friendship request between user with id: " + idSource + " and user with id: " + idDestination + "!");
        }
        return this.friendShipRequestService.addFriendShipRequest(idSource, idDestination, status, date);
    }

    public Iterable<FriendshipRequest> getAllFriendshipRequests(){
        return this.friendShipRequestService.getAll();
    }

    public FriendshipRequest removeFriendshipRequest(Long id){
        return this.friendShipRequestService.removeFriendshipRequest(id);
    }

    public FriendshipRequest getFriendshipRequest(Long id){
        return this.friendShipRequestService.getFriendshipRequest(id);
    }

    public int getFriendshipRequestsSize(){
        return this.friendShipRequestService.getFriendshipRequestsSize();
    }


    public Iterable<FriendshipRequest> getFriendshipRequestsOfAnUser(Long id) {
        Predicate<FriendshipRequest> predicateFilterFriendshipRequests = x-> x.getIdDestination().equals(id);
        List<FriendshipRequest> friendshipRequestsToReturn = StreamSupport.stream(this.getAllFriendshipRequests().spliterator(), false)
                .filter(predicateFilterFriendshipRequests)
                .collect(Collectors.toList());
        return friendshipRequestsToReturn;
    }

    public Iterable<FriendshipRequest> getPendingFriendshipRequestsOfAnUser(Long id) {
        Predicate<FriendshipRequest> predicateFilterFriendshipRequests = x-> x.getIdDestination().equals(id) && x.getStatusFriendshipRequest().equals(StatusFrierndshipRequest.PENDING);
        List<FriendshipRequest> friendshipRequestsToReturn = StreamSupport.stream(this.getAllFriendshipRequests().spliterator(), false)
                .filter(predicateFilterFriendshipRequests)
                .collect(Collectors.toList());
        return friendshipRequestsToReturn;
    }

    public void updateStatusFriendshipRequest(FriendshipRequest oldFR, String status){
        FriendshipRequest newFR = new FriendshipRequest(oldFR.getIdSource(), oldFR.getIdDestination(), status, oldFR.getDate());
        newFR.setId(oldFR.getId());
        this.friendShipRequestService.updateStatusFriendshipRequest(newFR);
    }

    public Message addMessage(User from, List<User> to, String message, LocalDateTime date, Long reply){
        return this.messageService.addMessage(from, to, message, date, reply);
    }

    public Iterable<Message> getAllMessage() {
        return this.messageService.getAll();
    }

    public Message getMessage(Long id){
        return this.messageService.getMessage(id);
    }

    public int getMessagesSize() {
        return this.messageService.getMessagesSize();
    }

    public Iterable<Message> getMessagesBetween(Long id1, Long id2) {
        return this.messageService.getMessagesBetween(this.getUser(id1), this.getUser(id2));
    }

    public List<User> convertIdListToUserList(String[] s) {
        MessageValidator.validateTolist(s);
        List<User> toReturn = new ArrayList<>();
        for(String id: s){
            User user = this.getUser(Long.parseLong(id));
            if(user==null){
                throw new ValidationException("The user with id " + id + " does not exist");
            }
            else{
                toReturn.add(user);
            }
        }
        return toReturn;
    }

    public void verifyUserWithAnIdExists(Long id1) {
        if(this.getUser(id1)==null){
            throw new ValidationException("The user with id: " + id1 + " does not exist!");
        }
    }
}
