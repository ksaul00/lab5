package socialnetwork;

import socialnetwork.UI.Console;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.FriendShipRequestValidator;
import socialnetwork.domain.validators.FriendShipValidator;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.DataBase.FriendShipDbRepository;
import socialnetwork.repository.DataBase.FriendShipRequestDbRepository;
import socialnetwork.repository.DataBase.MessageDataBase;
import socialnetwork.repository.DataBase.UserDbRepository;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.FriendShipFileRepository;
import socialnetwork.repository.file.UserFileRepository;
import socialnetwork.service.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Repository<Long, User> userRepository;
        Repository<Tuple<Long, Long>, FriendShip> friendRepository;
        Repository<Long, FriendshipRequest> friendshipRequestRepository;
        Repository<Long, Message> messageRepository;
        Service service;

        userRepository = new UserDbRepository("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new UtilizatorValidator());
        friendRepository = new FriendShipDbRepository("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new FriendShipValidator());
        friendshipRequestRepository = new FriendShipRequestDbRepository("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new FriendShipRequestValidator());
        messageRepository = new MessageDataBase("jdbc:postgresql://localhost:5432/lab", "postgres", "parola1234", new MessageValidator(), userRepository);

        UserService userService = new UserService(userRepository);
        FriendShipService friendShipService = new FriendShipService(friendRepository);
        FriendShipRequestService friendShipRequestService = new FriendShipRequestService(friendshipRequestRepository);
        MessageService messageService = new MessageService(messageRepository);

        service = new Service(userService, friendShipService, friendShipRequestService, messageService);
        Console console = new Console(service);
        console.runMenu();
    }
}
