package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;

public class Message extends Entity<Long> {
    User from;
    List<User> to;
    String message;
    LocalDateTime date;
    Long reply;

    public Message(User from, List<User> to, String message, LocalDateTime date,Long reply) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        this.reply=reply;
    }

    public User getFrom() {
        return from;
    }

    public List<User> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Long getReply() {
        return reply;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public void setTo(List<User> to) {
        this.to = to;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setReply(Long reply) {
        this.reply = reply;
    }
}