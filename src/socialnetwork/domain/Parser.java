package socialnetwork.domain;

public class Parser {
    public static StatusFrierndshipRequest parseStatusFriendshipRequest(String sfr){
        if(sfr.equals("PENDING"))
            return StatusFrierndshipRequest.PENDING;
        else if(sfr.equals("APPROVED"))
            return StatusFrierndshipRequest.APPROVED;
        else if(sfr.equals("REJECTED"))
            return StatusFrierndshipRequest.REJECTED;
        else
            return null;
    }
}
