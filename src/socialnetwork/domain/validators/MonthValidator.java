package socialnetwork.domain.validators;

import java.time.Month;

public class MonthValidator implements Validator<String>{
    @Override
    public void validate(String month) throws ValidationException {
        boolean isARealMonth = false;
        for(int i = 1 ; i <= 12 ; i++)
            if(month.equals(Month.of(i).toString())){
                isARealMonth = true;
                break;
            }
        if(!isARealMonth)
            throw new ValidationException(month + " isn't a valid month! ");
    }
}
