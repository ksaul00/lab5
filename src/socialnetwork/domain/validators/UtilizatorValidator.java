package socialnetwork.domain.validators;

import socialnetwork.domain.User;

import static java.lang.Character.*;

public class UtilizatorValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        this.validateUserName(entity.getUsername());
        this.validateFirstName(entity.getFirstName());
        this.validateLastName(entity.getLastName());
        this.validatePassword(entity.getPassword());
    }

    private void validateUserName(String userName){
        if(userName.isEmpty()){
            throw new ValidationException("User must have a UserName!");
        }
        int litere=0;
        char c;
        if(userName.length()<5){
            throw new ValidationException("UserName must contains at least 5 characters!");
        }
        for (int i = 0; i < userName.length(); i++){
            c = userName.charAt(i);
            if(isLetter(c)){
                litere++;
            }
            else{
                if(c == ' '){
                    throw new ValidationException("UserName can't contains spaces!");
                }
            }
        }
        if(litere<3){
            throw new ValidationException("UserName must contains at least 3 letters!");
        }
    }

    private void validatePassword(String password){
        if(password.isEmpty()){
            throw new ValidationException("User must have a password!");
        }
        int litereMari=0;
        int litere=0;
        int numere=0;
        char c;
        if(password.length()<8){
            throw new ValidationException("Password must contains at least 8 characters!");
        }
        for (int i = 0; i < password.length(); i++){
            c = password.charAt(i);
            if(isLetter(c)){
                if(c==toUpperCase(c)){
                    litereMari++;
                }
                litere++;
            }
            else if(isDigit(c)){
                    numere++;
                }
        }
        if(litereMari<1){
            throw new ValidationException("Password must contains at least 1 upper letters!");
        }
        if(litere<3){
            throw new ValidationException("UserName must contains at least 3 letters!");
        }
        if(numere<2){
            throw new ValidationException("UserName must contains at least 2 digit numbers!");
        }
    }

    private void validateLastName(String lastName) {
        if(lastName.isEmpty()){
            throw new ValidationException("User must have a last name!");
        }
        char c = lastName.charAt(0);
        if(isLetter(c)){
            if(c!=toUpperCase(c)){
                throw new ValidationException("The first letter must be upper in Last name");
            }
        }
        else{
            throw new ValidationException("Only letters can be used in Last Name");
        }
        for (int i = 1; i < lastName.length(); i++){
            c = lastName.charAt(i);
            if(isLetter(c)){
                if(c!=toLowerCase(c)){
                    throw new ValidationException("Only the first letter can de upper in Last name");
                }
            }
            else{
                throw new ValidationException("Only letters can be used in Last Name");
            }
        }
    }

    private void validateFirstName(String firstName) throws ValidationException{
        if(firstName.isEmpty()){
            throw new ValidationException("User must have a first name!");
        }
        char c = firstName.charAt(0);
        if(isLetter(c)){
            if(c!=toUpperCase(c)){
                throw new ValidationException("The first letter must be upper in first name");
            }
        }
        else{
            throw new ValidationException("Only letters can be used in First Name");
        }
        for (int i = 1; i < firstName.length(); i++){
            c = firstName.charAt(i);
            if(isLetter(c)){
                if(c!=toLowerCase(c)){
                    throw new ValidationException("Only the first letter can de upper in Frist Name");
                }
            }
            else{
                throw new ValidationException("Only letters can be used in First Name");
            }
        }
    }
}
