package socialnetwork.domain.validators;

import socialnetwork.domain.FriendShip;

public class FriendShipValidator implements Validator<FriendShip> {
    @Override
    public void validate(FriendShip entity) throws ValidationException {
        if(entity.getId().getLeft().equals(entity.getId().getRight())){
            throw new ValidationException("User can't be his own friend!");
        }
    }
}
