package socialnetwork.domain.validators;

import socialnetwork.domain.FriendshipRequest;

public class FriendShipRequestValidator implements Validator<FriendshipRequest>{
    @Override
    public void validate(FriendshipRequest entity) throws ValidationException {
        if(entity.getIdSource().equals(entity.getIdDestination())){
            throw new ValidationException("User can't be his own friend!");
        }
    }
}
