package socialnetwork.domain.validators;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MessageValidator implements Validator<Message>{
    @Override
    public void validate(Message entity) throws ValidationException {
        validateMessage(entity.getMessage());
    }

    public static void validateTolist(String[] lista){
        Set<String> set = new HashSet<String>();
        for(String id: lista){
            set.add(id);
        }
        if(set.size() < lista.length){
            throw new ValidationException("There is a user introduced more than once");
        }
    }

    private void validateMessage(String msg){
        if(msg.isEmpty()){
            throw new ValidationException("Message can't be empty!");
        }
    }
}
