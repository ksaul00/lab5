package socialnetwork.domain;


import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendShip extends Entity<Tuple<Long,Long>> {

    private LocalDate date;

    public FriendShip(LocalDate date) {
        this.date = date;
    }

    public FriendShip(){
        this.date = null;
    }

    public LocalDate getDate() {
        return date;
    }



    @Override
    public boolean equals(Object obj) {
        return this.getId().equals(((FriendShip) obj).getId());
    }

    @Override
    public String toString() {
        return "FriendShip{" + this.getId().getLeft() + ", " + this.getId().getRight()+ " since " + this.date + "}";
    }
}
