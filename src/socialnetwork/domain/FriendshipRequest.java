package socialnetwork.domain;

import java.time.LocalDate;

public class FriendshipRequest extends Entity<Long> {
    Long idSource;
    Long idDestination;
    StatusFrierndshipRequest statusFrierndshipRequest;
    private LocalDate date;

    public FriendshipRequest(Long idSource, Long idDestination, String statusFrierndshipRequest, LocalDate data) {
        this.idSource = idSource;
        this.idDestination = idDestination;
        this.statusFrierndshipRequest = Parser.parseStatusFriendshipRequest(statusFrierndshipRequest);
        this.date = data;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getIdSource() {
        return idSource;
    }

    public void setIdSource(Long idSource) {
        this.idSource = idSource;
    }

    public Long getIdDestination() {
        return idDestination;
    }

    public void setIdDestination(Long idDestination) {
        this.idDestination = idDestination;
    }

    public StatusFrierndshipRequest getStatusFriendshipRequest() {
        return statusFrierndshipRequest;
    }

    public void setStatusFrierndshipRequest(StatusFrierndshipRequest statusFrierndshipRequest) {
        this.statusFrierndshipRequest = statusFrierndshipRequest;
    }

    @Override
    public String toString() {
        return "FriendshipRequest{" +
                "id='" + this.getId() + '\'' +
                "idSource=" + idSource +
                ", idDestination=" + idDestination +
                ", statusFrierndshipRequest=" + statusFrierndshipRequest +
                ", date=" + date +
                '}';
    }
}
