package socialnetwork.domain;

public enum StatusFrierndshipRequest {
    PENDING,
    APPROVED,
    REJECTED;
}
