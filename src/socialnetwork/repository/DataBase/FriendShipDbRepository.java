package socialnetwork.repository.DataBase;

import socialnetwork.domain.FriendShip;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;

public class FriendShipDbRepository implements Repository<Tuple<Long, Long>, FriendShip> {
    private String url;
    private String username;
    private String password;
    private Validator<FriendShip> validator;

    public FriendShipDbRepository(String url, String username, String password, Validator<FriendShip> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public FriendShip findOne(Tuple<Long, Long> id) {
        if (id == null)
            throw new IllegalArgumentException("The id cannot be null");

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement("SELECT * from friendships where (id1 = ?, id2 = ?)");
        ) {
            ps.setLong(1, id.getLeft());
            ps.setLong(1, id.getRight());
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()){
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                LocalDate date = resultSet.getDate("friendship_since").toLocalDate();
                FriendShip friendShip = new FriendShip(date);
                friendShip.setId(id);
                return friendShip;
            }
            else{
                return null;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

        @Override
    public Iterable<FriendShip> findAll() {
        Set<FriendShip> friendShips = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                LocalDate date = resultSet.getDate("friendship_since").toLocalDate();

                Tuple<Long, Long> id = new Tuple<>(id1, id2);
                FriendShip friendShip = new FriendShip(date);
                friendShip.setId(id);
                friendShips.add(friendShip);
            }
            return friendShips;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendShips;
    }

    @Override
    public FriendShip save(FriendShip entity) {
        if(entity==null)
            throw new IllegalArgumentException("The entity cannot be null");
        validator.validate(entity);

        String sql = "insert into friendships (id1, id2, friendship_since) values (?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getId().getLeft());
            ps.setLong(2, entity.getId().getRight());
            ps.setDate(3, Date.valueOf(entity.getDate()));

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendShip delete(Tuple<Long, Long> longLongTuple) {
        if(longLongTuple==null)
            throw new IllegalArgumentException("The entity cannot be null");
        String sql = "delete from friendships where (id1 = ? and id2 = ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, longLongTuple.getLeft());
            ps.setLong(2, longLongTuple.getRight());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendShip update(FriendShip entity) {
        if (entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);
        String sql = "update friendships, friendship_since = ?  where id1 = ? and id2 = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql))
        {
            ps.setDate(3, Date.valueOf(entity.getDate()));
            ps.setLong(1, entity.getId().getLeft());
            ps.setLong(2, entity.getId().getRight());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
}


    @Override
    public int getSize() {
        int size=0;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT Count(*) AS NumarFriendships from friendships");
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                size=resultSet.getInt("NumarFriendships");
            }
            return size;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return size;
    }

    @Override
    public Set<Tuple<Long, Long>> getIDs() {
        Set<Tuple<Long, Long>> ids = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                ids.add(new Tuple<>(id1, id2));
            }
            return ids;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ids;
    }
}
