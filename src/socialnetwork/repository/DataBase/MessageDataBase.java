package socialnetwork.repository.DataBase;

import socialnetwork.domain.Entity;
import socialnetwork.domain.FriendShip;
import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MessageDataBase implements Repository<Long, Message> {
    private String url;
    private String username;
    private String password;
    private MessageValidator validator;
    Repository<Long,User> userRepo;

    public MessageDataBase(String url, String username, String password, MessageValidator messageValidator, Repository<Long,User> userRepo) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = messageValidator;
        this.userRepo = userRepo;
    }

    @Override
    public Message save(Message entity) {
        if(entity==null)
            throw new IllegalArgumentException("The entity cannot be null");
        validator.validate(entity);
        String sql="insert into Messages (fromuser, textmessage, date_message, time_message, reply) values (?, ?, ?, ?, ?)";
        String sqlList="insert into messageuserlist (id_message, id_user) values (?, ?)";
        String sqlGetId="select id from messages where textmessage=? and reply=? and date_message=? and time_message=?";
        try(Connection connection= DriverManager.getConnection(url,username,password);
            PreparedStatement statement=connection.prepareStatement(sql)){

            statement.setLong(1,entity.getFrom().getId());
            statement.setString(2,entity.getMessage());
            statement.setDate(3, Date.valueOf(entity.getDate().toLocalDate()));
            statement.setTime(4,Time.valueOf(entity.getDate().toLocalTime()));
            Long replied=entity.getReply();
            statement.setLong(5,replied);
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }

        Long messageId=0l;
        try(Connection connection= DriverManager.getConnection(url,username,password);
            PreparedStatement statement=connection.prepareStatement(sqlGetId)){
            Long replied=entity.getReply();
            statement.setString(1,entity.getMessage());
            statement.setLong(2,replied);
            statement.setDate(3,Date.valueOf(entity.getDate().toLocalDate()));
            statement.setTime(4,Time.valueOf(entity.getDate().toLocalTime()));
            ResultSet resultSet=statement.executeQuery();
            resultSet.next();
            messageId=resultSet.getLong("id");
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        try(Connection connection= DriverManager.getConnection(url,username,password);
            PreparedStatement statement2=connection.prepareStatement(sqlList)){
            statement2.setLong(1,messageId);
            entity.getTo().forEach(x->{
                try {
                    statement2.setLong(2,x.getId());
                    statement2.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });

            entity.setId(null);
            return entity;

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        return null;
    }

    @Override
    public Message delete(Long aLong) {

        if(aLong==null)
            throw new IllegalArgumentException("The entity cannot be null");
        Message task = findOne(aLong);
        if(task!=null){
            String sql = "delete from messages where id = ?";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setLong(1, aLong);
                ps.executeUpdate();
                return task;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
        else{
            throw new ValidationException("Message with ID: " + aLong + " does not exist!");
        }
    }

    @Override
    public Message update(Message entity) {
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public Set<Long> getIDs() {
        return null;
    }

    @Override
    public Message findOne(Long aLong) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages where id = ?");
             PreparedStatement statement1 = connection.prepareStatement("select * from messageuserlist where id_message=?")) {

            statement.setLong(1, aLong);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("id");
                Long idFrom = resultSet.getLong("fromuser");
                String text = resultSet.getString("textmessage");
                Date date = resultSet.getDate("date_message");
                Time time = resultSet.getTime("time_message");
                Long reply=resultSet.getLong("reply");

                statement1.setLong(1,id);
                ResultSet listaUseri=statement1.executeQuery();
                List<User> lista=new ArrayList<>();
                while(listaUseri.next()){
                    Long idUser=listaUseri.getLong("id_user");
                    User nouUser=userRepo.findOne(idUser);
                    lista.add(nouUser);
                }

                User from = userRepo.findOne(idFrom);
                Message nouMessage=new Message(from,lista,text,LocalDateTime.of(date.toLocalDate(),time.toLocalTime()),reply);
                nouMessage.setId(id);
                return nouMessage;

            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Message> findAll() {
        Set<Message> messages=new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages");
             PreparedStatement statement1 = connection.prepareStatement("select * from messageuserlist where id_message=?")) {

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("id");
                Long idFrom = resultSet.getLong("fromuser");
                String text = resultSet.getString("textmessage");
                Date date = resultSet.getDate("date_message");
                Time time = resultSet.getTime("time_message");
                Long reply=resultSet.getLong("reply");

                statement1.setLong(1,id);
                ResultSet listaUseri=statement1.executeQuery();
                List<User> lista=new ArrayList<>();
                while(listaUseri.next()){
                    Long idUser=listaUseri.getLong("id_user");
                    User nouUser=userRepo.findOne(idUser);
                    lista.add(nouUser);
                }

                User from = userRepo.findOne(idFrom);
                Message nouMessage=new Message(from,lista,text,LocalDateTime.of(date.toLocalDate(),time.toLocalTime()),reply);
                nouMessage.setId(id);
                messages.add(nouMessage);
            }
            return messages;
        } catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}

