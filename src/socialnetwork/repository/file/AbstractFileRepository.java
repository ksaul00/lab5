package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.io.*;
import java.util.Arrays;
import java.util.List;

///Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factory class (vezi mai jos)
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {
    String fileName;

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    private void loadData() {
        try(BufferedReader reader=new BufferedReader(new FileReader(fileName))) {
            String newLine;
            while((newLine=reader.readLine())!=null){
                //System.out.println(newLine);
                List<String> data= Arrays.asList(newLine.split(";"));
                E entity=extractEntity(data);
                super.save(entity);
            }

        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public abstract E extractEntity(List<String> attributes);

    protected abstract String createEntityAsString(E entity);

    @Override
    public E save(E entity) {
        super.save(entity);
        writeToFile(entity);
        return entity;
    }

    @Override
    public E delete(ID id) {
        E e = super.delete(id);
        if(e==null){
            return null;
        }
        writeEntities();
        return e;
    }

    @Override
    public E update(E entity) {
        if (super.update(entity) != null)
            return entity;
        writeEntities();
        return null;
    }

    private void writeEntities(){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))){
            for (E e : findAll()){
                bufferedWriter.write(createEntityAsString(e));
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void writeToFile(E entity) {
        try(BufferedWriter writer=new BufferedWriter(new FileWriter(fileName,true))) {

            writer.write(createEntityAsString(entity));
            writer.newLine();

        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }




}
