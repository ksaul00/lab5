package socialnetwork.repository.file;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.file.AbstractFileRepository;

import java.util.List;

public class UserFileRepository extends AbstractFileRepository<Long, User> {

    public UserFileRepository(String fileName, Validator<User> validator) {
        super(fileName, validator);
    }

    @Override
    public User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1),attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }

    private User getUser(Long id) {
        for(User user: this.findAll()){
            if(user.getId().equals(id)){
                return user;
            }
        }
        throw new ValidationException("The user whith id: " + id + " doesn't exist");
    }

    @Override
    protected String createEntityAsString(User entity) {
        //String friends = "";
        //for(int i=0;i<entity.getFriends().size();i++){
        //    friends = friends + " " + entity.getFriends().get(i).getId();
        //}

        return entity.getId()+";"+entity.getFirstName()+";"+entity.getLastName();//+";"+friends;
    }




}
