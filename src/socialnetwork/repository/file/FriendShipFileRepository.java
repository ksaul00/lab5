package socialnetwork.repository.file;

import socialnetwork.domain.FriendShip;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.util.List;

public class FriendShipFileRepository extends socialnetwork.repository.file.AbstractFileRepository<Tuple<Long, Long>, FriendShip> {{
}

    public FriendShipFileRepository(String fileName, Validator<FriendShip> validator) {
        super(fileName, validator);
    }

    @Override
    public FriendShip extractEntity(List<String> attributes) {
        FriendShip friendShip = new FriendShip();
        friendShip.setId(new Tuple<Long, Long>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1))));
        return friendShip;
    }

    @Override
    protected String createEntityAsString(FriendShip entity) {
        return entity.getId().getLeft()+";"+entity.getId().getRight();
    }

}
